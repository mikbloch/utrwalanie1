package com.utrwalanie.projekt;

import com.utrwalanie.projekt.service.HSQLDBSchema;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjektApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjektApplication.class, args);
		HSQLDBSchema.Create();
	}


}
