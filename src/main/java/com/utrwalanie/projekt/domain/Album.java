package com.utrwalanie.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor @AllArgsConstructor
public class Album {
    private int ID;
    private String Title;
    private Date ReleaseDate;

    public Album(String title, Date releaseDate) {
        Title = title;
        ReleaseDate = releaseDate;
    }
}
