package com.utrwalanie.projekt.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor @AllArgsConstructor
public class Song {
    private int ID;
    private String Title;
    private int Length;
    private boolean Explicit;
    private int AlbumID;

    public Song(String title, int length, Boolean explicit, int albumID) {
        Title = title;
        Length = length;
        Explicit = explicit;
        AlbumID = albumID;
    }
}