package com.utrwalanie.projekt.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class HSQLDBConnection {

    private Connection connection;

    public Connection getCon() {
        try {
            String  DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
            connection = DriverManager.getConnection(DB_URL);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void closeCon() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
