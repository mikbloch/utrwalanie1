package com.utrwalanie.projekt.service;

import com.utrwalanie.projekt.domain.Album;
import com.utrwalanie.projekt.domain.Song;

import java.util.List;

public interface SongService {
    public void addSong(Song song);

    public void addSongs(List<Song> songs);

    public Song selectSong(int ID);

    public Song selectSongByTitle(String Title);

    public List<Song> selectSongs();

    public void updateSong(Song song);

    public void deleteSong(int ID);

    public void deleteAll();

}
