package com.utrwalanie.projekt.service;

import com.utrwalanie.projekt.domain.Album;

import java.util.List;

public interface AlbumService {

    public void addAlbum(Album album);

    public void addAlbums(List<Album> albums);

    public Album selectAlbum(int ID);

    public Album selectAlbumByTitle(String name);

    public List<Album> selectAlbums();

    public void updateAlbum(Album album);

    public void deleteAlbum(int ID);

    public void deleteAll();
}
