package com.utrwalanie.projekt.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class HSQLDBSchema {

    static public void Create() {

        final String CREATE = "DROP TABLE IF EXISTS Song;" +
                "DROP TABLE IF EXISTS Album;" +
                "CREATE TABLE Album (" +
                "ID INTEGER IDENTITY PRIMARY KEY," +
                "Title VARCHAR(50) UNIQUE NOT NULL," +
                "ReleaseDate DATE NOT NULL" +
                ");" +
                "CREATE TABLE Song(" +
                "ID INTEGER IDENTITY PRIMARY KEY," +
                "Title VARCHAR(50) NOT NULL," +
                "Length INT NOT NULL," +
                "isExplicit BOOLEAN NOT NULL," +
                "AlbumID INT NOT NULL," +
                "FOREIGN KEY (AlbumID) REFERENCES Album(ID)" +
                ");";

        try {
            String  DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
            Connection connection = DriverManager.getConnection(DB_URL);
            Statement CreateST = connection.createStatement();
            CreateST.executeUpdate(CREATE);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
