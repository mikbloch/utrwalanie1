package com.utrwalanie.projekt.service;

import com.utrwalanie.projekt.domain.Song;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SongServiceJDBC implements SongService{

    HSQLDBConnection con = new HSQLDBConnection();

    public void addSong(Song Song) {
        String ADD_SONG = "INSERT INTO Song(title, length, isexplicit, albumid) VALUES(?, ?, ?, ?)";
        Connection c = con.getCon();
        try {
            PreparedStatement addPST = c.prepareStatement(ADD_SONG);

            addPST.setString(1, Song.getTitle());
            addPST.setInt(2, Song.getLength());
            addPST.setBoolean(3, Song.isExplicit());
            addPST.setInt(4, Song.getAlbumID());
            addPST.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        con.closeCon();
    }

    public void addSongs(List<Song> Songs) {
        String ADD_SONG = "INSERT INTO Song(title, length, isexplicit, albumid) VALUES(?, ?, ?, ?)";
        Connection c = con.getCon();
        try {
            c.setAutoCommit(false);
            PreparedStatement addPST = c.prepareStatement(ADD_SONG);
            for (Song Song: Songs) {
                addPST.setString(1, Song.getTitle());
                addPST.setInt(2, Song.getLength());
                addPST.setBoolean(3, Song.isExplicit());
                addPST.setInt(4, Song.getAlbumID());
                addPST.executeUpdate();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            c.commit();
        }
        catch (SQLException e) {
                try {
                c.rollback();
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
        }
        con.closeCon();
    }

    public Song selectSong(int ID) {
        String FIND_SONGS = "SELECT id, title, length, isexplicit, albumid FROM Song WHERE ID = %d";
        Song Song = null;
        Connection c = con.getCon();

        try {
            PreparedStatement findPST = c.prepareStatement(String.format(FIND_SONGS, ID));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("Title");
                int length = rs.getInt("Length");
                boolean isexplicit = rs.getBoolean("isexplicit");
                int albumid = rs.getInt("albumid");
                Song = new Song(id, title, length, isexplicit, albumid);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return Song;
    }

    public Song selectSongByTitle(String Title) {
        String FIND_SONGS = "SELECT id, title, length, isexplicit, albumid FROM Song WHERE Title = '%s'";
        Song song = null;
        Connection c = con.getCon();

        try {
            PreparedStatement findPST = c.prepareStatement(String.format(FIND_SONGS, Title));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("Title");
                int length = rs.getInt("Length");
                boolean isexplicit = rs.getBoolean("isexplicit");
                int albumid = rs.getInt("albumid");
                song = new Song(id, title, length, isexplicit, albumid);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return song;
    }

    public List<Song> selectSongs() {
        String FIND_SONGS = "SELECT id, title, length, isexplicit, albumid FROM Song";
        List<Song> Songs = new ArrayList<Song>();
        Connection c = con.getCon();

        try {
            PreparedStatement findPST = c.prepareStatement(String.format(FIND_SONGS));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String title = rs.getString("Title");
                int length = rs.getInt("Length");
                boolean isexplicit = rs.getBoolean("isexplicit");
                int albumid = rs.getInt("albumid");

                Song Song = new Song(id, title, length, isexplicit, albumid);
                Songs.add(Song);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return Songs;
    }

    public void updateSong(Song song) {
        String UPDATE_SONG = "UPDATE Song SET title = '%s', length = %d, " +
            "isexplicit = %s , albumid = %d WHERE Id = %d";
        Connection c = con.getCon();
        try {
            PreparedStatement updatePST = c.prepareStatement(String.format(UPDATE_SONG, song.getTitle(),
                    song.getLength(), song.isExplicit(), song.getAlbumID(), song.getID()));
            updatePST.executeUpdate();
        }
        catch ( SQLException e) {
            e.printStackTrace();
        }
        con.closeCon();
    }

    public void deleteSong(int ID) {
        Connection c = con.getCon();
        String DELETE_Song = "DELETE FROM Song WHERE ID = %d";
        try {
            PreparedStatement deletePST = c.prepareStatement(String.format(DELETE_Song, ID));
            deletePST.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        con.closeCon();
    }

    public void deleteAll() {
        Connection c = con.getCon();
        try {
            Statement deleteST = c.createStatement();
            deleteST.executeUpdate("DELETE FROM Song");
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        con.closeCon();
    }

    public int TotalExplicitSong() {
        Connection c = con.getCon();
        int count = -1;
        String SELECT_SONG = "Select Count(id) count FROM song WHERE song.isexplicit;";
        try {
            PreparedStatement findPST = c.prepareStatement(String.format(SELECT_SONG));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                count = rs.getInt("count");
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return count;
    }

    public float AvarageSongLength() {
        Connection c = con.getCon();
        float result = -1f;
        String SELECT_SONG = "Select Avg(length) result FROM song;";
        try {
            PreparedStatement findPST = c.prepareStatement(SELECT_SONG);
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                result = rs.getFloat("result");
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }
}
