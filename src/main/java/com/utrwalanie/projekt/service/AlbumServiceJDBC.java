package com.utrwalanie.projekt.service;

import com.utrwalanie.projekt.domain.Album;
import org.hsqldb.SqlInvariants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AlbumServiceJDBC implements AlbumService {

    HSQLDBConnection con = new HSQLDBConnection();


    public void addAlbum(Album album) {
        String ADD_ALBUM = "INSERT INTO Album(title, releasedate) VALUES(?, ?)";
        Connection c = con.getCon();
        try {
            PreparedStatement addPST = c.prepareStatement(ADD_ALBUM);

            addPST.setString(1, album.getTitle());
            addPST.setDate(2, album.getReleaseDate());
            addPST.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        con.closeCon();
    }

    public void addAlbums(List<Album> albums) {
        String ADD_ALBUM = "INSERT INTO Album(title, releasedate) VALUES(?, ?)";
        Connection c = con.getCon();
        try {
            c.setAutoCommit(false);
            PreparedStatement addPST = c.prepareStatement(ADD_ALBUM);
            for (Album album: albums) {
                addPST.setString(1, album.getTitle());
                addPST.setDate(2, album.getReleaseDate());
                addPST.executeUpdate();
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            c.commit();
        }
        catch (SQLException e) {
            try {
                c.rollback();
            }
            catch (SQLException e2) {
                e2.printStackTrace();
            }
        }
        con.closeCon();
    }

    public Album selectAlbum(int ID) {
        String FIND_ALBUMS = "SELECT id, title, releasedate FROM Album WHERE ID = %d";
        Album album = null;
        Connection c = con.getCon();

        try {
            PreparedStatement findPST = c.prepareStatement(String.format(FIND_ALBUMS, ID));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String Title = rs.getString("Title");
                Date releaseDate = rs.getDate("ReleaseDate");

                album = new Album(id, Title, releaseDate);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return album;
    }

    public Album selectAlbumByTitle(String title) {
        String FIND_ALBUMS = "SELECT id, title, releasedate FROM Album WHERE Title = '%s'";
        Album album = null;
        Connection c = con.getCon();

        try {
            PreparedStatement findPST = c.prepareStatement(String.format(FIND_ALBUMS, title));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String Title = rs.getString("Title");
                Date releaseDate = rs.getDate("ReleaseDate");

                album = new Album(id, Title, releaseDate);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return album;
    }


    public List<Album> selectAlbums() {
        String FIND_ALBUMS = "SELECT id, title, releasedate FROM Album";
        List<Album> albums = new ArrayList<Album>();
        Connection c = con.getCon();

        try {
            PreparedStatement findPST = c.prepareStatement(String.format(FIND_ALBUMS));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String Title = rs.getString("Title");
                Date releaseDate = rs.getDate("ReleaseDate");

                Album album = new Album(id, Title, releaseDate);
                albums.add(album);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return albums;
    }


    public void updateAlbum(Album album) {
        String UPDATE_ALBUM = "UPDATE Album SET Title = '%s', ReleaseDate = '%s' WHERE Id = %d";
        Connection c = con.getCon();
        try {
            PreparedStatement updatePST = c.prepareStatement(String.format(UPDATE_ALBUM, album.getTitle(),
                    album.getReleaseDate(), album.getID()));
            updatePST.executeUpdate();
        }
        catch ( SQLException e) {
            e.printStackTrace();
        }
        con.closeCon();
    }

    public void deleteAlbum(int ID) {
        Connection c = con.getCon();
        String DELETE_ALBUM = "DELETE FROM ALBUM WHERE ID = %d";
        try {
            PreparedStatement deletePST = c.prepareStatement(String.format(DELETE_ALBUM, ID));
            deletePST.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        con.closeCon();
    }

    public void deleteAll() {
        Connection c = con.getCon();
        try {
            Statement deleteST = c.createStatement();
            deleteST.executeUpdate("DELETE FROM ALBUM");
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        con.closeCon();
    }

    public List<String[]> albumSongCount() {
        Connection c = con.getCon();
        List<String[]> albums = new ArrayList<>();
        String SELECT_ALBUMS = "Select a.id, a.Title, Count(s.id) songs from album a inner join song s on a.id = s.albumid GROUP BY a.id ORDER BY Title;";
        try {
            PreparedStatement findPST = c.prepareStatement(SELECT_ALBUMS);
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                String[] album = new String[3];
                album[0] = String.valueOf(rs.getInt("id"));
                album[1] = rs.getString("Title");
                album[2] = String.valueOf(rs.getInt("songs"));
                albums.add(album);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }
        con.closeCon();
        return albums;
    }

    public Album NewestAlbum() {
        String FIND_ALBUM = "SELECT TOP 1 id, title, releasedate FROM Album ORDER BY releasedate DESC";
        Album album = null;
        Connection c = con.getCon();

        try {
            PreparedStatement findPST = c.prepareStatement(String.format(FIND_ALBUM));
            ResultSet rs = findPST.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("id");
                String Title = rs.getString("Title");
                Date releaseDate = rs.getDate("ReleaseDate");

                album = new Album(id, Title, releaseDate);
            }
        }
        catch (SQLException e){
            e.printStackTrace();
        }

        return album;

    }
}
