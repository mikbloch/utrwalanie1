package com.utrwalanie.projekt;


import com.utrwalanie.projekt.domain.Album;
import com.utrwalanie.projekt.domain.Song;
import com.utrwalanie.projekt.service.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class AlbumServiceTests {

	HSQLDBSchema schema = new HSQLDBSchema();
	AlbumServiceJDBC as = new AlbumServiceJDBC();
	SongServiceJDBC ss = new SongServiceJDBC();

	@BeforeClass
	void Schema() { schema.Create(); }

	@BeforeEach
	void clean() {
		ss.deleteAll();
		as.deleteAll();
	}

	@Test
	void Create() {
		Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
		Album a2 = new Album("Ye", Date.valueOf("2018-06-01"));

		as.addAlbum(a1);
		as.addAlbum(a2);

		assertEquals(2,  as.selectAlbums().size());
	}

	@Test
	void Read() {
		Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
		as.addAlbum(a1);
		assertNotEquals(null, as.selectAlbumByTitle(a1.getTitle()));
	}

	@Test
	void Update() {
		Album album = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
		as.addAlbum(album);
		Album a1 = as.selectAlbumByTitle("Jesus Is King");

		assertNotEquals(null, as.selectAlbumByTitle("Jesus Is King"));
		assertEquals(null, as.selectAlbumByTitle("Ye"));
		a1.setTitle("Ye");
		as.updateAlbum(a1);
		assertEquals(null, as.selectAlbumByTitle("Jesus Is King"));
		assertNotEquals(null, as.selectAlbumByTitle("Ye"));

	}

	@Test
	void Delete() {
		Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
		Album a2 = new Album("Ye", Date.valueOf("2018-06-01"));
		Album a3 = new Album("The Life Of Pablo", Date.valueOf("2016-02-31"));
		as.addAlbum(a1);
		as.addAlbum(a2);
		as.addAlbum(a3);

		assertEquals(3, as.selectAlbums().size());
		as.deleteAlbum(as.selectAlbumByTitle("Ye").getID());
		assertEquals(2, as.selectAlbums().size());
		as.deleteAll();
		assertEquals(0, as.selectAlbums().size());
	}

	@Test
	void SongCountQuery() {
		Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
		as.addAlbum(a1);
		int ida1 = as.selectAlbumByTitle(a1.getTitle()).getID();
		Song s1 = new Song("Selah", 320, false, ida1);
		Song s2 = new Song("Jesus Is Lord", 210, false, ida1);
		Song s3 = new Song("Everything We Need", 123, false, ida1);
		ss.addSong(s1);
		ss.addSong(s2);
		ss.addSong(s3);

		Album a2 = new Album("Ye", Date.valueOf("2018-07-05"));
		as.addAlbum(a2);
		int ida2 = as.selectAlbumByTitle(a2.getTitle()).getID();
		Song s12 = new Song("Yiekes", 320, false, ida2);
		Song s22 = new Song("All Mine", 210, true, ida2);
		ss.addSong(s12);
		ss.addSong(s22);

		List<String[]> albums = as.albumSongCount();
		System.out.println(albums.get(0)[1]);
		assertEquals(2, albums.size());
		assertTrue(albums.get(0)[1].equals(a1.getTitle()));
		assertTrue(albums.get(0)[2].equals("3"));
		assertTrue(albums.get(1)[1].equals(a2.getTitle()));
		assertTrue(albums.get(1)[2].equals("2"));

	}

	@Test
	void NewestAlbumQuery(){
		Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
		Album a2 = new Album("Ye", Date.valueOf("2018-06-01"));
		Album a3 = new Album("The Life Of Pablo", Date.valueOf("2016-02-31"));
		as.addAlbum(a2);
		as.addAlbum(a1);
		as.addAlbum(a3);

		Album album = as.NewestAlbum();

		assertEquals(a1.getReleaseDate(), album.getReleaseDate());
	}
}
