package com.utrwalanie.projekt;

import com.utrwalanie.projekt.domain.Album;
import com.utrwalanie.projekt.domain.Song;
import com.utrwalanie.projekt.service.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import static org.junit.jupiter.api.Assertions.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class SongServiceTest {

    AlbumServiceJDBC as = new AlbumServiceJDBC();
    SongServiceJDBC ss = new SongServiceJDBC();

    @BeforeEach
    void clean() {
        ss.deleteAll();
        as.deleteAll();
    }

    @Test
    void Create() {
        Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));

        as.addAlbum(a1);

        int ida1 = as.selectAlbumByTitle(a1.getTitle()).getID();

        Song s1 = new Song("Selah", 320, false, ida1);
        Song s2 = new Song("Jesus Is Lord", 210, false, ida1);
        Song s3 = new Song("Everything We Need", 123, false, ida1);

        List<Song> songs= new ArrayList<>();

        songs.add(s1);
        songs.add(s2);
        songs.add(s3);

        ss.addSongs(songs);

        assertEquals(3,  ss.selectSongs().size());
    }

    @Test
    void Read() {
        Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
        as.addAlbum(a1);
        Song s1 = new Song("Selah", 320, false, as.selectAlbumByTitle(a1.getTitle()).getID());
        ss.addSong(s1);

        assertNotEquals(null, ss.selectSongByTitle(s1.getTitle()));
    }


    @Test
    void Update() {
        Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
        as.addAlbum(a1);
        Song song = new Song("Selah", 320, false, as.selectAlbumByTitle(a1.getTitle()).getID());
        ss.addSong(song);
        Song s1 = ss.selectSongByTitle(song.getTitle());
        assertNotEquals(null, ss.selectSongByTitle("Selah"));
        assertEquals(null, ss.selectSongByTitle("Jesus Is King"));
        s1.setTitle("Jesus Is King");
        ss.updateSong(s1);
        assertEquals(null, ss.selectSongByTitle("Selah"));
        assertNotEquals(null, ss.selectSongByTitle("Jesus Is King"));
    }

    @Test
    void Delete() {
        Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
        as.addAlbum(a1);
        int ida1 = as.selectAlbumByTitle(a1.getTitle()).getID();
        Song s1 = new Song("Selah", 320, false, ida1);
        Song s2 = new Song("Jesus Is Lord", 210, false, ida1);
        Song s3 = new Song("Everything We Need", 123, false, ida1);
        ss.addSong(s1);
        ss.addSong(s2);
        ss.addSong(s3);

        assertEquals(3, ss.selectSongs().size());
        ss.deleteSong(ss.selectSongByTitle("Selah").getID());
        assertEquals(2, ss.selectSongs().size());
        ss.deleteAll();
        assertEquals(0, ss.selectSongs().size());
    }

    @Test
    void TotalExplicitSongQuery() {
        Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
        as.addAlbum(a1);
        int ida1 = as.selectAlbumByTitle(a1.getTitle()).getID();
        Song s1 = new Song("Selah", 300, false, ida1);
        Song s2 = new Song("Jesus Is Lord", 300, false, ida1);
        Song s3 = new Song("Everything We Need", 200, true, ida1);
        ss.addSong(s1);
        ss.addSong(s2);
        ss.addSong(s3);

        Album a2 = new Album("Ye", Date.valueOf("2018-07-05"));
        as.addAlbum(a2);
        int ida2 = as.selectAlbumByTitle(a2.getTitle()).getID();
        Song s12 = new Song("Yiekes", 200, false, ida2);
        Song s22 = new Song("All Mine", 300, true, ida2);
        ss.addSong(s12);
        ss.addSong(s22);

        int total = ss.TotalExplicitSong();
        assertEquals(2, total);
    }

    @Test
    void AvarageSongLengthQuery(){
        Album a1 = new Album("Jesus Is King", Date.valueOf("2019-07-05"));
        as.addAlbum(a1);
        int ida1 = as.selectAlbumByTitle(a1.getTitle()).getID();
        Song s1 = new Song("Selah", 300, false, ida1);
        Song s2 = new Song("Jesus Is Lord", 300, false, ida1);
        Song s3 = new Song("Everything We Need", 200, true, ida1);
        ss.addSong(s1);
        ss.addSong(s2);
        ss.addSong(s3);

        Album a2 = new Album("Ye", Date.valueOf("2018-07-05"));
        as.addAlbum(a2);
        int ida2 = as.selectAlbumByTitle(a2.getTitle()).getID();
        Song s12 = new Song("Yiekes", 200, false, ida2);
        Song s22 = new Song("All Mine", 300, true, ida2);
        ss.addSong(s12);
        ss.addSong(s22);

        float result = ss.AvarageSongLength();
        assertEquals(260f, result);
    }
}
