package com.utrwalanie.projekt;

import com.utrwalanie.projekt.domain.Album;
import com.utrwalanie.projekt.domain.Song;
import com.utrwalanie.projekt.service.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class DomainTests {

    int aId = 1;
    String aTitle = "Jesus Is King";
    String aTitle2 = "Ye";
    Date aDate = Date.valueOf("2019-07-05");

    int sId = 1;
    String sTitle = "";
    int sLength = 300;
    int sLength2 = 400;
    boolean sExplicit = false;
    int sAId = 1;

    Album a1 = new Album(aId, aTitle, aDate);
    Song s1 = new Song(sId, sTitle, sLength, sExplicit, sAId);

    @Test
    void getters() {
        assertEquals(a1.getID(), aId);
        assertEquals(a1.getTitle(), aTitle);
        assertEquals(a1.getReleaseDate(), aDate);

        assertEquals(s1.getID(), sId);
        assertEquals(s1.getTitle(), sTitle);
        assertEquals(s1.getLength(), sLength);
        assertEquals(s1.isExplicit(), sExplicit);
        assertEquals(s1.getAlbumID(), sAId);
    }

    @Test
    void setters() {
       assertNotEquals(a1.getTitle(), aTitle2);
       assertNotEquals(s1.getLength(), sLength2);

       a1.setTitle(aTitle2);
       s1.setLength(sLength2);

       assertEquals(a1.getTitle(), aTitle2);
       assertEquals(s1.getLength(), sLength2);

       a1.setTitle(aTitle);
       s1.setLength(sLength);
    }

    @Test
    void equals() {
        Album a2 = new Album(aId, aTitle, aDate);
        Song s2 = new Song(sId, sTitle, sLength, sExplicit, sAId);

        assertEquals(a1, a2);
        assertEquals(s1, s2);
    }
}
